local resolution = 75;

local frames(x, y=1) = {
  start: x * y + 1,
  end: (x * y) + y,
};

local param_job(x) =
  {
    image: 'nytimes/blender:3.0-gpu-ubuntu18.04',
    stage: 'deploy',
    // cache: {
    //   key: '$CI_COMMIT_REF_SLUG',
    //   paths: [
    //     'tmp',
    //   ],
    //   policy: 'pull-push',
    // },

    script: [
      // 'ls fluid_cache',
      'blender -b MoonWalk.blend --python render_file.py -- --resolution ' + resolution + ' --frame ' + frames(x).start + ' --enable-addon',
    ],
    artifacts: {
      paths: [
        'tmp/',
      ],
    },
    // cache: {
    //   key: 'fluid_cache',
    //   paths: [
    //     'fluid_cache/',
    //   ],
    //   policy: 'pull',
    // },
  };

local arr = std.range(0, 249);


{


  // fluidBuild: {
  //   image: 'etews/gcc-cmake-python:latest',
  //   stage: '.pre',
  //   script: [
  //     'apt-get update && apt-get -y upgrade',
  //     'apt-get -y install git zip',
  //     'which cmake',
  //     'CMAKE_PATH=$(which cmake)',
  //     'MAKE_PATH=$(which make)',
  //     'git clone https://github.com/rlguy/Blender-FLIP-Fluids.git flip',
  //     'mv material_library flip/src/addon/materials/material_library',
  //     'cd flip',
  //     'mv cmake/CMakeLists.txt CMakeLists.txt',
  //     'mkdir build',
  //     'cd build',
  //     'cmake .. -DBUILD_DEBUG=ON',
  //     'make',
  //     'cmake .. -DBUILD_DEBUG=OFF',
  //     'make',
  //     'cd ../..',
  //     'mv flip/build/bl_flip_fluids/flip_fluids_addon flip_fluids_addon',
  //     'zip -r flip_fluids_addon.zip flip_fluids_addon',
  //   ],
  //   cache: [],
  //   artifacts: {
  //     paths: [
  //       'flip_fluids_addon.zip',
  //     ],
  //   },

  // },

  // rspec: param_job(0),
  // 'rspec 2': param_job(1),
  // 'rspec 3': param_job(2),
  // fluidBake: {
  //   image: 'nytimes/blender:3.0-gpu-ubuntu18.04',
  //   // needs: [
  //   //   {
  //   //     job: 'fluidBuild',
  //   //     artifacts: true,
  //   //   },
  //   // ],
  //   stage: 'build',
  //   script: [
  //     'blender -b RiotLogov2.blend --python bpy_activate_addon.py',
  //     'ls fluid_cache',
  //   ],
  //   cache: {
  //     key: 'fluid_cache',
  //     paths: [
  //       'fluid_cache/',
  //     ],
  //     policy: 'push',
  //   },
  //   // TODO: Add back in once tested
  //   rules: [
  //     {
  //       changes: [
  //         'RiotLogov2.blend',
  //       ],
  //     },

  //   ],
  //   // artifacts: {
  //   //   paths: [
  //   //     'fluid_cache',
  //   //   ],
  //   // },
  // },
  bundle: {
    image: 'alpine:latest',
    stage: '.post',
    script: [
      'ls',
      'apk add -U zip',
      'rm -rf build',
      'mv tmp build',
      'zip build.zip build',
    ],
    artifacts: {
      paths: [
        'build.zip',
      ],
    },
  },


} + {
  ['render_' + sd]: param_job(sd)
  for sd in arr
}
