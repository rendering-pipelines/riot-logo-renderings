import bpy

# Register the addon and enable it
bpy.ops.preferences.addon_install(filepath='./flip_fluids_addon.zip')
bpy.ops.preferences.addon_enable(module='flip_fluids_addon')
