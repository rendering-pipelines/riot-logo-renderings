

import bpy

# parser.add_argument("-b")

def enable_addon():
    # bpy.ops.preferences.addon_install(filepath='./flip_fluids_addon.zip')
    # bpy.ops.preferences.addon_enable(module='flip_fluids_addon')
    bpy.ops.preferences.addon_enable(module='pose_library')

def set_frame(args):
    
    frame = args.frame
    print(frame)
    bpy.context.scene.frame_start = frame
    bpy.context.scene.frame_end = frame

def set_resolution(args):
    resolution = args.resolution
    bpy.context.scene.render.resolution_percentage = resolution

def set_path(args):
    path = args.path
    bpy.context.scene.render.filepath = path

def render_frame():
    bpy.ops.render.render(animation=True)

def main():
    import argparse  # to parse options for us and print a nice help message
    import sys  # to get command line args

    # get the args passed to blender after "--", all of which are ignored by
    # blender so scripts may receive their own arguments
    argv = sys.argv

    if "--" not in argv:
        argv = []  # as if no args are passed
    else:
        argv = argv[argv.index("--") + 1:]  # get all args after "--"
    parser = argparse.ArgumentParser(description="Render Blender File")
    parser.add_argument("--resolution", type=int, default=100, help="resolution percentage for the output")
    parser.add_argument("--frame", type=int, default = 1, help="frame to render")
    parser.add_argument("--path", type=str, default = "//tmp/", help="path to output files")
    parser.add_argument("--enable-addon", action="store_true", help="should addon be enabled before start")
    # parse arguments
    # bpy.app.debug_wm = True
    args = parser.parse_args(argv)
    if not argv:
        parser.print_help()
        return

    # if not args.text:
    #     print("Error: --text=\"some string\" argument not given, aborting.")
    #     parser.print_help()
    #     return
    # set args
    if args.enable_addon:
        enable_addon()
    set_frame(args)
    set_resolution(args)
    set_path(args)
    render_frame()
    # print(args)


if __name__ == "__main__":
    main()
